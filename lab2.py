from sklearn import svm
import matplotlib.pyplot as plt

klasyfikator = svm.SVC()
X = [[2,3], [3,4], [5,6]]
c = [1, 0, 1]
klasyfikator.fit(X,c)

print(klasyfikator.predict([2, 3]))

def classify(X):
    Xs = []
    ys = []
    for[x,y,c] in X:
        if c==1 or c == 0:
            Xs.append([x,y])
            ys.append(c)
    clf = svm.SVC()
    clf.fit(Xs, ys)
    for i in range(len(X)):
        if X[i][2] == -1:
            X[i][2] = clf.predict([X[i][0], X[i][1]])
    return(X)



def load_data(filename):
    X = []
    for line in open(filename):
        tokens= line.split()
        X.append([float(tokens[0]), float(tokens[1]), float(tokens[2])])
    return(X)

def draw(X):

    x1 = []
    y1 = []
    x0 = []
    y0 = []
    xx = []
    yy = []
    c = []

    #f = open ('test')
    # for line in f:
    #      if float(line.split()[2])==1:
    #           x1.append(float(line.split()[0]))
    #           y1.append(float(line.split()[1]))
    #
    #       if float(line.split()[2]) == 0:
    #           x0.append(float(line.split()[0]))
    #           y0.append(float(line.split()[1]))
    #
    #       if float(line.split()[2]) == -1:
    #           xx.append(float(line.split()[0]))
    #           yy.append(float(line.split()[1]))
    #       c.append(float(line.split()[2]))
    for [x,y,c] in X:
        if c == 1:
            x1.append(x)
            y1.append(y)
        if c == 0 :
            x0.append(x)
            y0.append(y)
        if c == -1 :
            xx.append(x)
            yy.append(y)

    plt.axis([-10,10,-10,10])
    plt. plot(x1,y1,'ro', color = 'blue')
    plt. plot(x0,y0,'ro', color = 'grey')
    plt. plot(xx,yy,'ro', color = 'red')

    plt.show()

X = load_data('test')
draw(classify(X))