from lab1 import tf_idf
from lab1 import feature_values
from sklearn import svm
from nltk.stem.porter import PorterStemmer
import re
from nltk.corpus import stopwords, reuters
from os import listdir
cachedStopWords = stopwords.words("english")
min_length = 3
negadocs= []
posdocs = []

class corpus:
    def __init__(self,dir_neg,dir_pos):
        self.dir_neg = dir_neg
        self.dir_pos = dir_pos
        self.documents = []
        for i, file in enumerate(listdir(dir_neg)):
            if i < 300:
                fs = open(dir_neg + "\\" + file, 'r')
                text = fs.read()
                positive = 0
                train = 0
                doc = document(text, positive, train)
                self.add_document(doc)
                # negdocs.append(open(dir_neg + "\\" + file,'r').read())
            else:
                fs = open(dir_neg + "\\" + file, 'r')
                text = fs.read()
                positive = 0
                train = 1
                doc = document(text, positive, train)
                self.add_document(doc)

            for i, file in enumerate(listdir(dir_pos)):
                if i < 300:
                    fs = open(dir_pos + "\\" + file, 'r')
                    text = fs.read()
                    positive = 1
                    train = 0
                    doc = document(text, positive, train)
                    self.add_document(doc)

                    # negdocs.append(open(dir_neg + "\\" + file,'r').read())
                else:
                    fs = open(dir_pos + "\\" + file, 'r')
                    text = fs.read()
                    positive = 1
                    train = 1
                    doc = document(text, positive, train)
                    self.add_document(doc)


    def add_document(self, document):
        self.documents.append(document)

    def get_train_documents(self):
        train = []
        for doc in self.documents:
            if doc.train == 1:
                train.append(doc.text)
        return train


    def initialize_vocalbulary(self):
        self.vocabulary = []
        self.inversevocabulary = []
        for i,doc in enumerate(self.documents):
            if i % 1000 == 0:
                print(i)
            for word in doc.get_unique_values():
                if word not in self.vocabulary:
                    self.vocabulary[i] = word
                    self.inversevocabulary[word] = i

class document:
    def __init__(self, text, positive =1 , train=1):
        self.positive = positive
        self.train = train
        self.text = text
    def propocessing(self, raw_tokens):
        no_stopwords = [token for token in raw_tokens if token not in cachedStopWords]

        stemmed_tokens = []
        stemmer = PorterStemmer()
        for token in no_stopwords:
            stemmed_tokens.append(stemmer.stem(token))
        p = re.compile('[a-zA-Z]+');
        pattern_checked = []
        for stem in stemmed_tokens:
            if p.match(stem) and len(stem) >= min_length:
                pattern_checked.append(stem)
        return pattern_checked
    def get_unique_words(self):
        word_list=[]
        raw_tokens=self.text.split()

        for word in self.propocessing(self.text.split()):
            if not word in word_list:
                word_list.append(word)
        return word_list

    def get_vector(self, inverse_vocabulary):
        lng = len(inverse_vocabulary)
        vector = [0 for i in range(lng)]
        for word in self.text.split():
            vector[inverse_vocabulary[word]] = 1
        return vector

class tf_idf:
    def __init__(self):
        self.D = 0.0
        self.df = []
    def add_document(self,document):
        self.D +=1
        for token in set(document):
            self.df[token] += 1.0
    def idf(self,token):
        return math.log(self.D/self.df[token])
    def tf(self, token,document):
        liczba_wystapien = 0.0
        liczba_tokenow = 0.0
        for t in document:
            liczba_tokenow +=1.0
            if t == token:
                liczba_wystapien += 1.0
        return liczba_wystapien/liczba_tokenow

klasyfikator = svm.SVC()
crp = corpus("home\\ada\\Downloads\\txt_sentoken\\neg", "home\\ada\\Downloads\\txt_sentoken\\pos")
crp.initialize_vocabulary()
print(crp.vocabulary)
print(crp.inverse_vocabulary["pirate"])
(X,y) = crp.get_svn_vectors(Train=1)
print("ssss")
klasyfikator.fit(X,y)


(XT,yt) = crp.get_svn_vectors(Test=1)
pozytywne = 0
wszystkie = 0
for i,x in enumerate(XT):
    wszystkie +=1
    klasa = klasyfikator.predict(x)
    if klasa == yt[i]:
        pozytywne = pozytywne +1

print(pozytywne)
print(wszystkie)
